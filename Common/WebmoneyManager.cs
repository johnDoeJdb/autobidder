﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoBidder.Database;
using AutoBidder.Models;
using Webmoney.DataClasses.Currencies;
using Webmoney.Exchanger;

namespace AutoBidder.Common
{
    public class WebmoneyManager
    {

        public void UpdateUsersBids()
        {
            List<UserProfile> userProfiles = GetAllValidUsers();

//            return userProfiles;
        }

        public List<UserProfile> GetAllValidUsers()
        {
            List<UserProfile> userProfiles;
            using (var db = new UsersContext())
            {
                userProfiles = db.UserProfiles.Where(u => !u.WebmoneyKey.Equals(string.Empty) && u.Wmid.Equals(string.Empty)).Select(u => u).ToList();
            }

            return userProfiles;
        }

        public void UpdateBids()
        {
            List<byte> exchangeTypes = GetExchangeTypesForUpdate();
            using (var db = new UsersContext())
            {
                ExchangerManager exchangerManager = new ExchangerManager();
                foreach (var exchangeType in exchangeTypes)
                {
                    var webmoneyBids = exchangerManager.GetTopListBids(exchangeType);
                    if (webmoneyBids.WMExchnagerQuerys != null && webmoneyBids.WMExchnagerQuerys.Queries.Any())
                    {
                        foreach (var query in webmoneyBids.WMExchnagerQuerys.Queries)
                        {
                            WebmoneyBid webmoneyBid = new WebmoneyBid();
                            webmoneyBid.Id = Convert.ToInt32(query.Id);
                            webmoneyBid.AllAmountIn = query.AllAmountIn;
                            webmoneyBid.AmountIn = query.AmountIn;
                            webmoneyBid.AmountOut = query.AmountOut;
                            webmoneyBid.InOutRate = query.InOutRate;
                            webmoneyBid.OutInRate = query.OutInRate;
                            webmoneyBid.AmountIn = query.AmountIn;
                            webmoneyBid.AmountOut = query.AmountOut;
                            webmoneyBid.QueryDate = query.QueryDate;
                            webmoneyBid.CurrencyIn = webmoneyBids.WMExchnagerQuerys.CurrencyIn;
                            webmoneyBid.CurrencyOut = webmoneyBids.WMExchnagerQuerys.CurrencyOut;
                            db.WebmoneyBids.Add(webmoneyBid);
                        }
                    }
                }
                db.SaveChanges();
            }
        }

        public void UpdateProfileBids()
        {
            using (var db = new UsersContext())
            {
                ExchangerManager exchangerManager = new ExchangerManager();
                var profileBids = db.WebmoneyProfiesBids.Where(b => b.Active).Select(b => b).Include(b => b.UserProfile).ToList();
                var topBids = db.WebmoneyBids.Select(b => b).ToList();
                foreach (var profileBid in profileBids)
                {
                    IEnumerable<WebmoneyBid> bidsWithSameCurrencyPair = topBids
                        .Where(b => b.CurrencyIn == profileBid.InCurrency && b.CurrencyOut == profileBid.OutCurrency);
                    if (bidsWithSameCurrencyPair.Any())
                    {
                        float minRate = bidsWithSameCurrencyPair.Min(b => b.OutInRate);
                        if (minRate < profileBid.CurrentOutInRate && minRate > profileBid.MinOutInRate)
                        {
                            float newRate = minRate - (float)(minRate * 0.001);
                            var userProfile = db.UserProfiles.Where(u => u.UserId == profileBid.UserProfile.UserId).SingleOrDefault();
                            if (userProfile != null)
                            {
                                var result = exchangerManager.ChangeRateForBid(userProfile.Wmid, profileBid.QueryId, newRate, userProfile.WebmoneyKey);
                                if (result.ReturnCode == 0)
                                {
                                    profileBid.CurrentOutInRate = newRate;
                                }
                            }
                        }
                    }
                }
                db.SaveChanges();
            }
        }

        public void ClearAllBids()
        {
            using (var db = new UsersContext())
            {
                var allBids = db.WebmoneyBids.Where(x => x.Id != 0);
                db.WebmoneyBids.RemoveRange(allBids);
                db.SaveChanges();
            }
        }

        private List<byte> GetExchangeTypesForUpdate()
        {
            List<byte> exchangeTypes = new List<byte>();
            using (var db = new UsersContext())
            {
                var distinctCurrencies = db.WebmoneyProfiesBids.Where(b => b.Active).Select(b => new { b.InCurrency, b.OutCurrency }).Distinct().ToList();
                foreach (var currencyPair in distinctCurrencies)
                {
                    var inCurrncy = (Currency)Enum.Parse(typeof(Currency), currencyPair.InCurrency);
                    var outCurrncy = (Currency)Enum.Parse(typeof(Currency), currencyPair.OutCurrency);
                    byte? type = ExchangeType.GetExchangeType(inCurrncy, outCurrncy);
                    if (type != null)
                    {
                        exchangeTypes.Add(type.GetValueOrDefault());
                    }
                }
            }

            return exchangeTypes;
        }
//
//        public List<WebmoneyProfieBid> GetAllActiveBids()
//        {
//            List<WebmoneyProfieBid> bids;
//            using (var db = new UsersContext())
//            {
//                userProfiles = db.WebmoneyProfiesBids.Where(u => !u.WebmoneyKey.Equals(string.Empty) && u.Wmid.Equals(string.Empty)).Select(u => u).ToList();
//            }
//
//            return userProfiles;
//        }


    }
}