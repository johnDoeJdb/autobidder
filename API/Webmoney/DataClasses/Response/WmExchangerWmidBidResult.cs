﻿using System;

namespace Webmoney.DataClasses.Response
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute("wm.exchanger.response", Namespace = "", IsNullable = false)]
    public class WmExchangerWmidBidResult
    {
        [System.Xml.Serialization.XmlElementAttribute("WMExchnagerQuerys")]
        public WmExchangerWmidBidResultQueries Queries { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("retval")]
        public int ReturnCode { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("retdesc")]
        public string ReturnDescription { get; set; }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class WmExchangerWmidBidResultQueries
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("query")]
        public WmExchangerWmidBidResultQuery[] Queries { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("wmid")]
        public ulong Wmid { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("type")]
        public byte Type { get; set; }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class WmExchangerWmidBidResultQuery
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("id")]
        public uint Id { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("exchtype")]
        public byte ExchangeType { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("state")]
        public byte State { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("amountin")]
        public double AmountIn { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("amountout")]
        public double AmountOut { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("inoutrate")]
        public float InOutRate { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("outinrate")]
        public float OutInRate { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("inpurse")]
        public string InPurse { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("outpurse")]
        public string OutPurse { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("querydatecr")]
        public DateTime DateCreated { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("querydate")]
        public DateTime QueryDate { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute("direction")]
        public string Direction { get ; set; }
    }


}