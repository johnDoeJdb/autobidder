﻿using System;
using System.Xml.Serialization;

namespace Webmoney.DataClasses.Response
{
    /// <remarks/>
    [XmlTypeAttribute(AnonymousType = true)]
    [XmlRootAttribute("wm.exchanger.response", Namespace = "", IsNullable = false)]
    public partial class WmExchangerBidListResult
    {
        [XmlElement("BankRate")]
        public WmExchangerBidListResultBankRate BankRate { get; set; }

        /// <remarks/>
        public WmExchangerBidListResultQueries WMExchnagerQuerys { get; set; }
    }

    /// <remarks/>
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class WmExchangerBidListResultBankRate
    {
        /// <remarks/>
        [XmlAttributeAttribute("direction")]
        public string Direction { get; set; }

        /// <remarks/>
        [XmlAttributeAttribute("ratetype")]
        public byte RateType { get; set; }

        /// <remarks/>
        [XmlTextAttribute()]
        public float Value { get; set; }
    }

    /// <remarks/>
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class WmExchangerBidListResultQueries
    {
        /// <remarks/>
        [XmlElementAttribute("query")]
        public WmExchangerBidListResultQuery[] Queries { get; set; }

        /// <remarks/>
        [XmlAttributeAttribute("amountin")]
        public string CurrencyIn { get; set; }

        /// <remarks/>
        [XmlAttributeAttribute("amountout")]
        public string CurrencyOut { get; set; }

        /// <remarks/>
        [XmlAttributeAttribute("inoutrate")]
        public string InOutCurrencyPair { get; set; }

        /// <remarks/>
        [XmlAttributeAttribute("outinrate")]
        public string OutInCurrencyPair { get; set; }
    }

    /// <remarks/>
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class WmExchangerBidListResultQuery
    {
        /// <remarks/>
        [XmlAttributeAttribute(AttributeName = "id")]
        public uint Id { get; set; }

        /// <remarks/>
        [XmlAttributeAttribute(AttributeName = "amountin")]
        public double AmountIn { get; set; }

        /// <remarks/>
        [XmlAttributeAttribute(AttributeName = "amountout")]
        public double AmountOut { get; set; }

        /// <remarks/>
        [XmlAttributeAttribute(AttributeName = "inoutrate")]
        public float InOutRate { get; set; }

        /// <remarks/>
        [XmlAttributeAttribute(AttributeName = "outinrate")]
        public float OutInRate { get; set; }

        /// <remarks/>
        [XmlAttributeAttribute("procentbankrate")]
        public string ProcentBankRate { get; set; }

        /// <remarks/>
        [XmlAttributeAttribute(AttributeName = "allamountin")]
        public double AllAmountIn { get; set; }

        /// <remarks/>
        [XmlAttributeAttribute(AttributeName  = "querydate")]
        public DateTime QueryDate { get; set; }
    }
}