﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Webmoney.DataClasses.Response
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute("wm.exchanger.response", Namespace = "", IsNullable = false)]
    public partial class WmExchangerCommonResult
    {
        [System.Xml.Serialization.XmlElementAttribute("retval")]
        public int ReturnCode { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("retdesc")]
        public string ReturnDescription { get; set; }
    }
}
