﻿using System;
using System.Linq;

namespace Webmoney.DataClasses.Currencies
{
    public static class ExchangeType
    {
        public static CurrencyPair DirectionToCurrencyPair(string direction)
        {
            CurrencyPair pair = new CurrencyPair();
            char[] delimiterChars = { '-' };
            string[] currencies = direction.Split(delimiterChars);
            if (currencies.Count() == 2)
            {
                pair.In = (Currency)System.Enum.Parse(typeof(Currency), currencies[0]);  
                pair.Out = (Currency)System.Enum.Parse(typeof(Currency), currencies[1]);  
            }

            return pair;
        }

        public static byte? GetExchangeType(Currency fromCurrency, Currency toCurrency)
        {
            byte? rateType = null;

            if (fromCurrency == Currency.WMZ && toCurrency == Currency.WMR)
            {
                rateType = 1;
            }
            else if (fromCurrency == Currency.WMR && toCurrency == Currency.WMZ)
            {
                rateType = 2;
            }
            else if (fromCurrency == Currency.WMZ && toCurrency == Currency.WME)
            {
                rateType = 3;
            }
            else if (fromCurrency == Currency.WME && toCurrency == Currency.WMZ)
            {
                rateType = 4;
            }
            else if (fromCurrency == Currency.WME && toCurrency == Currency.WMR)
            {
                rateType = 5;
            }
            else if (fromCurrency == Currency.WMR && toCurrency == Currency.WME)
            {
                rateType = 6;
            }
            else if (fromCurrency == Currency.WMZ && toCurrency == Currency.WMU)
            {
                rateType = 7;
            }
            else if (fromCurrency == Currency.WMU && toCurrency == Currency.WMZ)
            {
                rateType = 8;
            }
            else if (fromCurrency == Currency.WMR && toCurrency == Currency.WMU)
            {
                rateType = 9;
            }
            else if (fromCurrency == Currency.WMU && toCurrency == Currency.WMR)
            {
                rateType = 10;
            }
            else if (fromCurrency == Currency.WMU && toCurrency == Currency.WME)
            {
                rateType = 11;
            }
            else if (fromCurrency == Currency.WME && toCurrency == Currency.WMU)
            {
                rateType = 12;
            }
//            else if (fromCurrency == Currency.WMU && toCurrency == Currency.WMZ)
//            {
//                rateType = 13;
//            }
//            else if (fromCurrency == Currency.WMU && toCurrency == Currency.WMZ)
//            {
//                rateType = 14;
//            }
//            else if (fromCurrency == Currency.WMU && toCurrency == Currency.WMZ)
//            {
//                rateType = 15;
//            }
//            else if (fromCurrency == Currency.WMU && toCurrency == Currency.WMZ)
//            {
//                rateType = 16;
//            }
            else if (fromCurrency == Currency.WMB && toCurrency == Currency.WMZ)
            {
                rateType = 17;
            }
            else if (fromCurrency == Currency.WMZ && toCurrency == Currency.WMB)
            {
                rateType = 18;
            }
            else if (fromCurrency == Currency.WMB && toCurrency == Currency.WME)
            {
                rateType = 19;
            }
            else if (fromCurrency == Currency.WME && toCurrency == Currency.WMB)
            {
                rateType = 20;
            }
//            else if (fromCurrency == Currency.WMU && toCurrency == Currency.WMZ)
//            {
//                rateType = 21;
//            }
//            else if (fromCurrency == Currency.WMU && toCurrency == Currency.WMZ)
//            {
//                rateType = 22;
//            }
            else if (fromCurrency == Currency.WMR && toCurrency == Currency.WMB)
            {
                rateType = 23;
            }
            else if (fromCurrency == Currency.WMB && toCurrency == Currency.WMR)
            {
                rateType = 24;
            }
            else if (fromCurrency == Currency.WMZ && toCurrency == Currency.WMG)
            {
                rateType = 25;
            }
            else if (fromCurrency == Currency.WMG && toCurrency == Currency.WMZ)
            {
                rateType = 26;
            }
            else if (fromCurrency == Currency.WME && toCurrency == Currency.WMG)
            {
                rateType = 27;
            }
            else if (fromCurrency == Currency.WMG && toCurrency == Currency.WME)
            {
                rateType = 28;
            }
            else if (fromCurrency == Currency.WMR && toCurrency == Currency.WMG)
            {
                rateType = 29;
            }
            else if (fromCurrency == Currency.WMG && toCurrency == Currency.WMR)
            {
                rateType = 30;
            }
            else if (fromCurrency == Currency.WMU && toCurrency == Currency.WMG)
            {
                rateType = 31;
            }
            else if (fromCurrency == Currency.WMG && toCurrency == Currency.WMU)
            {
                rateType = 32;
            }
           
            return rateType;
        }
    }

    public enum Currency
    {
        WMB,
        WMC,
        WMD,
        WME,
        WMG,
        WMK,
        WMR,
        WMU,
        WMX,
        WMY,
        WMZ,
    }

    public class CurrencyPair
    {
        public Currency In
        {
            get
            {
                var val = (Currency)Enum.Parse(typeof(Currency), InCurrency);
                return val;
            }
            set
            {
                InCurrency = value.ToString();
            }
        }
        public Currency Out
        {
            get
            {
                var val = (Currency)Enum.Parse(typeof(Currency), OutCurrency);
                return val;
            }
            set
            {
                OutCurrency = value.ToString();
            }
        }
        
        public string InCurrency;
        public string OutCurrency;
    }
}
