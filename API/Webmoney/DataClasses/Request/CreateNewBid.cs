﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Webmoney.DataClasses.Request
{

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute("wm.exchanger.request", Namespace = "", IsNullable = false)]
    public partial class CreateNewBid
    {

        private object wmidField;

        private object signstrField;

        private object inpurseField;

        private object outpurseField;

        private object inamountField;

        private object outamountField;

        private object capitallerwmidField;

        /// <remarks/>
        public object wmid
        {
            get
            {
                return this.wmidField;
            }
            set
            {
                this.wmidField = value;
            }
        }

        /// <remarks/>
        public object signstr
        {
            get
            {
                return this.signstrField;
            }
            set
            {
                this.signstrField = value;
            }
        }

        /// <remarks/>
        public object inpurse
        {
            get
            {
                return this.inpurseField;
            }
            set
            {
                this.inpurseField = value;
            }
        }

        /// <remarks/>
        public object outpurse
        {
            get
            {
                return this.outpurseField;
            }
            set
            {
                this.outpurseField = value;
            }
        }

        /// <remarks/>
        public object inamount
        {
            get
            {
                return this.inamountField;
            }
            set
            {
                this.inamountField = value;
            }
        }

        /// <remarks/>
        public object outamount
        {
            get
            {
                return this.outamountField;
            }
            set
            {
                this.outamountField = value;
            }
        }

        /// <remarks/>
        public object capitallerwmid
        {
            get
            {
                return this.capitallerwmidField;
            }
            set
            {
                this.capitallerwmidField = value;
            }
        }
    }


}
