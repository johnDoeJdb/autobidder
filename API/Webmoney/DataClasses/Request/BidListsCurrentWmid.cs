﻿namespace Webmoney.DataClasses.Request
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute("wm.exchanger.request", Namespace = "", IsNullable = false)]
    public partial class RequestListBidsForWmid
    {

        private object wmidField;

        private object signstrField;

        private object typeField;

        private object queryidField;

        private object capitallerwmidField;

        /// <remarks/>
        public object wmid
        {
            get
            {
                return this.wmidField;
            }
            set
            {
                this.wmidField = value;
            }
        }

        /// <remarks/>
        public object signstr
        {
            get
            {
                return this.signstrField;
            }
            set
            {
                this.signstrField = value;
            }
        }

        /// <remarks/>
        public object type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public object queryid
        {
            get
            {
                return this.queryidField;
            }
            set
            {
                this.queryidField = value;
            }
        }

        /// <remarks/>
        public object capitallerwmid
        {
            get
            {
                return this.capitallerwmidField;
            }
            set
            {
                this.capitallerwmidField = value;
            }
        }
    }
}
