﻿namespace Webmoney.DataClasses.Request
{
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute("wm.exchanger.request", Namespace = "", IsNullable = false)]
    public partial class DeleteWmidBid
    {

        private object wmidField;

        private object signstrField;

        private object operidField;

        private object capitallerwmidField;

        /// <remarks/>
        public object wmid
        {
            get
            {
                return this.wmidField;
            }
            set
            {
                this.wmidField = value;
            }
        }

        /// <remarks/>
        public object signstr
        {
            get
            {
                return this.signstrField;
            }
            set
            {
                this.signstrField = value;
            }
        }

        /// <remarks/>
        public object operid
        {
            get
            {
                return this.operidField;
            }
            set
            {
                this.operidField = value;
            }
        }

        /// <remarks/>
        public object capitallerwmid
        {
            get
            {
                return this.capitallerwmidField;
            }
            set
            {
                this.capitallerwmidField = value;
            }
        }
    }


}
