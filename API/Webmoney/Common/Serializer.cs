﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;
using Webmoney.DataClasses.Response;

namespace Webmoney.Common
{
    static class Serializer
    {
        internal static string XmlSerialize<T>(this T objectToSerialize)
        {
            XmlWriterSettings settings = new XmlWriterSettings()
            {
                ConformanceLevel = ConformanceLevel.Fragment,
                OmitXmlDeclaration = false,
                Indent = true,
                NamespaceHandling = NamespaceHandling.OmitDuplicates
            };

            XmlSerializer serializer = new XmlSerializer(typeof(T));
            StringWriter stringWriter = new StringWriter();
            XmlWriter writer = XmlWriter.Create(stringWriter, settings);
            writer.WriteWhitespace("");

            serializer.Serialize(writer, objectToSerialize);
            string xml = stringWriter.ToString();

            return xml;
        }

        internal static T XmlDeserialize<T>(this string xmlString)
        {
            T returnValue = default(T);

            XmlSerializer serial = new XmlSerializer(typeof(T));
            StringReader reader = new StringReader(xmlString);
            object result = serial.Deserialize(reader);

            if (result != null && result is T)
            {
                returnValue = ((T)result);
            }

            reader.Close();

            return returnValue;
        }

        internal static WmExchangerCommonResult XmlDeserializeCommonResponse(string xmlString)
        {
            XmlRootAttribute xRoot = new XmlRootAttribute();
            xRoot.ElementName = "wm.exchanger.response";
            xRoot.IsNullable = true;

            XmlSerializer ser = new XmlSerializer(typeof(WmExchangerCommonResult), xRoot);
            XmlReader xRdr = XmlReader.Create(new StringReader(xmlString));
            WmExchangerCommonResult result = (WmExchangerCommonResult)ser.Deserialize(xRdr);

            return result;
        }  
    }
}
