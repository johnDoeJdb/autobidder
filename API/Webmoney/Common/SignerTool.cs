﻿using WebMoney.Cryptography;

namespace Webmoney.Common
{
    static class SignerTool
    {
        internal static string Sign(string xmlKey, string text)
        {
            Signer signer = new Signer();
            signer.Initialize(xmlKey);
            string signedText = signer.Sign(text, false);

            return signedText;
        }
    }
}
