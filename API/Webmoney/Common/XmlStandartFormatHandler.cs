﻿using System;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using Webmoney.DataClasses.Response;

namespace Webmoney.Common
{
    static class XmlStandartFormatHandler
    {
        public static string HandleXml(string xml)
        {
            var resultXml = ToStandartFloatNumbersFormat(xml);
            resultXml = ToStandartDateFormat(resultXml);

            return resultXml;
        }

        static string ToStandartFloatNumbersFormat(string xml)
        {
            string pattern2 = @"([+-]?)(\d+),(\d.*?)";
            Regex rgx = new Regex(pattern2);
            string resultXml = rgx.Replace(xml, @"$1$2.$3");

            return resultXml;
        }

        static string ToStandartDateFormat(string xml)
        {
            string pattern2 = @"""(\d{2}.\d{2}.\d{4}\s\d{1,2}:\d{2}:\d{2})""";
            Regex rgx = new Regex(pattern2);
            var evaluator = new MatchEvaluator(MatchToStandartXmlDateFormat);
            string resultXml = rgx.Replace(xml, evaluator);

            return resultXml;
        }

        private static string MatchToStandartXmlDateFormat(Match match)
        {
            string date = match.Groups[1].ToString();
            string[] formats = {"dd.MM.yyyy HH:mm:ss", "dd.MM.yyyy H:mm:ss"};
            var datetime = DateTime.ParseExact(date, formats , null, DateTimeStyles.None);
            string dateInXmlFormat = string.Format(@"""{0}""", datetime.ToString("s"));

            return dateInXmlFormat;
        }
    }
}
