﻿using System.Text.RegularExpressions;

namespace Webmoney.Common
{
    static class Validator
    {
        internal static bool isValidPurse(string purse)
        {
            return !Regex.IsMatch(purse, @"^[a-zA-Z]{1}\w{8-15}$");
        }
    }
}
