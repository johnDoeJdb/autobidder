﻿using System.Net;
using RestSharp;
using Webmoney.Common;
using Webmoney.DataClasses.Request;
using Webmoney.DataClasses.Response;

namespace Webmoney.Exchanger
{
    public class ExchangerManager
    {
        public void GetBestListBids()
        {
            
        }

        public WmExchangerBidListResult GetTopListBids(byte type = 1)
        {
            var client = new RestClient("https://wmeng.exchanger.ru");
            var request = new RestRequest("asp/XMLWMList.asp?exchtype={type}", Method.GET);
//            request.AddParameter("exchtype", type); // adds to POST or URL querystring based on Method
            request.AddUrlSegment("type", type.ToString()); // replaces matching token in request.Resource
            IRestResponse response = client.Execute(request);
            var resultXml = response.Content; // raw content as string
            resultXml = XmlStandartFormatHandler.HandleXml(resultXml);
            var result = Serializer.XmlDeserialize<WmExchangerBidListResult>(resultXml);

            return result;
        }

        public WmExchangerWmidBidResult ListWmidBids(long wmid, byte? type, int? queryId, string xmlKey)
        {
            type = type.HasValue ? type : 3;
            RequestListBidsForWmid request = new RequestListBidsForWmid();

            string sign = string.Format("{0}{1}{2}", wmid, type, queryId);
            request.capitallerwmid = string.Empty;
            request.queryid = queryId;
            request.wmid = wmid;
            request.type = type;
            request.signstr = SignerTool.Sign(xmlKey, sign);

            string xml = Serializer.XmlSerialize(request);
            WebClient httpClient = new WebClient();
            string resultXml = httpClient.UploadString("https://wm.exchanger.ru/asp/XMLWMList2.asp", "POST", xml);

            resultXml = XmlStandartFormatHandler.HandleXml(resultXml);
            var result = Serializer.XmlDeserialize<WmExchangerWmidBidResult>(resultXml);

            return result;
        }

        public WmExchangerCommonResult DeleteWmidBid(long wmid, long operationId, string xmlKey)
        {
            DeleteWmidBid request = new DeleteWmidBid();

            string sign = string.Format("{0}{1}", wmid, operationId);
            request.capitallerwmid = string.Empty;
            request.operid = operationId;
            request.wmid = wmid;
            request.signstr = SignerTool.Sign(xmlKey, sign);

            string xml = Serializer.XmlSerialize(request);
            WebClient httpClient = new WebClient();
            string resultXml = httpClient.UploadString("https://wm.exchanger.ru/asp/XMLTransDel.asp", "POST", xml);
            WmExchangerCommonResult result = Serializer.XmlDeserializeCommonResponse(resultXml);

            return result;
        }

        public WmExchangerCommonResult ChangeRateForBid(long wmid, long operationId, float exchangeRateValue, string xmlKey)
        {
            ChangeRateForBid request = new ChangeRateForBid();
            var stringaRate = exchangeRateValue.ToString("R");
            var rateType = 1; // OurInRate
            string sign = string.Format("{0}{1}{2}{3}", wmid, operationId, rateType, stringaRate);
            request.capitallerwmid = string.Empty;
            request.operid = operationId;
            request.cursamount = stringaRate;
            request.curstype = rateType;
            request.wmid = wmid;
            request.signstr = SignerTool.Sign(xmlKey, sign);

            string xml = Serializer.XmlSerialize(request);
            WebClient httpClient = new WebClient();
            string resultXml = httpClient.UploadString("https://wm.exchanger.ru/asp/XMLTransIzm.asp", "POST", xml);
            WmExchangerCommonResult result = Serializer.XmlDeserializeCommonResponse(resultXml);

            return result;
        }

        // Need implement Response data object
        //        public BidListsCurrentWmidResult CreateNewBid(long wmid, string inputPurse, float intputAmount, string outputPurse, float outputAmount, string xmlKey)
        //        {
        //            if (!Validator.isValidPurse(inputPurse) || !Validator.isValidPurse(outputPurse))
        //            {
        //                throw new Exception("Invalid purse format");
        //            }
        //            CreateNewBid request = new CreateNewBid();
        //
        //            string sign = string.Format("{0}{1}{2}{3}{4}", wmid, inputPurse, outputPurse, intputAmount, outputAmount);
        //            request.capitallerwmid = string.Empty;
        //            request.inpurse = inputPurse;
        //            request.inamount = intputAmount;
        //            request.outpurse = outputPurse;
        //            request.outamount = outputAmount;
        //            request.wmid = wmid;
        //            request.signstr = SignerTool.Sign(xmlKey, sign);
        //
        //            string xml = Serializer.XmlSerialize(request);
        //            WebClient httpClient = new WebClient();
        //            string resultXml = httpClient.UploadString("https://wm.exchanger.ru/asp/XMLTrustPay.asp", "POST", xml);
        //            BidListsCurrentWmidResult result = Serializer.XmlDeserialize<BidListsCurrentWmidResult>(resultXml);
        //
        //            return result;
        //        }
    }
}
