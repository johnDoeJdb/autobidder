﻿using System.Data.Entity;
using AutoBidder.Database;

namespace AutoBidder.Models
{
    public class UsersContext : DbContext
    {
        public UsersContext()
            : base("DefaultConnection")
        {
        }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<WebmoneyProfieBid> WebmoneyProfiesBids { get; set; }
        public DbSet<WebmoneyBid> WebmoneyBids { get; set; }
    }
}
