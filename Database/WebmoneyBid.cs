﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AutoBidder.Database
{
    [Table("WebmoneyBids")]
    public class WebmoneyBid
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string CurrencyIn { get; set; }
        public string CurrencyOut { get; set; }
        public double AmountIn { get; set; }
        public double AmountOut { get; set; }
        public float InOutRate { get; set; }
        public float OutInRate { get; set; }
        public double AllAmountIn { get; set; }
        public DateTime QueryDate { get; set; }
    }
}
