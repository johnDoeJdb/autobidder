﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Webmoney.DataClasses.Currencies;

namespace AutoBidder.Database
{
    [Table("WebmoneyProfieBids")]
    public class WebmoneyProfieBid
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long QueryId { get; set; }
        public bool Active { get; set; }
        public float MinOutInRate { get; set; }
        public float CurrentOutInRate { get; set; }
        public string InCurrency { get; set; }
        public string OutCurrency { get; set; }
        public UserProfile UserProfile { get; set; }
    }
}
