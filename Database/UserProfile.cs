﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AutoBidder.Database
{
    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }

        public string WebmoneyKey
        {
            get
            {
                string result = string.Empty;
                if (!string.IsNullOrEmpty(WebmoneyKeyData))
                {
                    byte[] data = Convert.FromBase64String(WebmoneyKeyData);
                    result = Encoding.UTF8.GetString(data);
                }

                return result;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    try
                    {
                        byte[] data = Convert.FromBase64String(value);
                        Encoding.UTF8.GetString(data);
                        WebmoneyKeyData = value;
                    }
                    catch (Exception e)
                    {
                        var plainTextBytes = Encoding.UTF8.GetBytes(value);
                        WebmoneyKeyData = Convert.ToBase64String(plainTextBytes);
                    }
                }
            }
        }

        public long Wmid { get; set; }

        private string WebmoneyKeyData;
    }
}
