﻿using System.Web.Mvc;
using AutoBidder.Filters;
using AutoBidder.Models;
using AutoBidder.Tasks;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace AutoBidder.Controllers
{
    [InitializeSimpleMembership]
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Scheduler.StartJobs();
            return View();
        }

        [HttpGet]
        public ActionResult AddNewBid()
        {
            var model = new ProfieWebmoneyModel();

            return View(model);
        }


        [HttpPost]
        public ActionResult AddNewBid(ProfieWebmoneyModel model)
        {
            if (ModelState.IsValid)
            {
                var result = model.Save();

                if (!result.IsSuccess && !string.IsNullOrEmpty(result.ErrorMessage))
                {
                    ModelState.AddModelError("Webmoney", result.ErrorMessage);
                }
            }
            else
            {
                return View("AddNewBid", model);
            }

            return RedirectToAction("Index");
        }

        public ActionResult GetWemoneyBids([DataSourceRequest]DataSourceRequest request)
        {
            var model = new ProfieWebmoneyModel();
            var bids = model.GetBids();

            return Json(bids.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateWemoneyBid(ProfieWebmoneyModel model)
        {
            if (ModelState.IsValid)
            {
                var result = model.Update();
                if (!result.IsSuccess && !string.IsNullOrEmpty(result.ErrorMessage))
                {
                    ModelState.AddModelError(string.Empty, result.ErrorMessage);
                }
            } 

            return Json(new[] { model }.ToDataSourceResult(new DataSourceRequest(), ModelState));
        }

        [HttpPost]
        public ActionResult DeleteWemoneyBid(ProfieWebmoneyModel model)
        {
            model.Delete();

            return Json(new[] { model }.ToDataSourceResult(new DataSourceRequest(), ModelState));
        }

        [HttpPost]
        public ActionResult CreateWemoneyBid(ProfieWebmoneyModel model)
        {
            if (ModelState.IsValid)
            {
                model.Save();
            }

            return Json(new[] { model }.ToDataSourceResult(new DataSourceRequest(), ModelState));
        }
    }
}
