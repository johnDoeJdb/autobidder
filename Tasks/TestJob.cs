﻿using AutoBidder.Common;
using Quartz;

namespace AutoBidder.Tasks
{
    public class BidRateChangeJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            WebmoneyManager webmoneyManager = new WebmoneyManager();
            webmoneyManager.ClearAllBids();
            webmoneyManager.UpdateBids();
            webmoneyManager.UpdateProfileBids();
        }
    }
}