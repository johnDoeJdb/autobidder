﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Security;
using AutoBidder.Common;
using AutoBidder.Database;
using DataAnnotationsExtensions;
using Webmoney.DataClasses.Currencies;

namespace AutoBidder.Models
{
    public class ProfieWebmoneyModel
    {
        [Required]
        [Min(1)]
        public int QueryId { get; set; }

        [Min(0.0000000000000000000000000000000000001)]
        [Required]
        public float MinOutInRate { get; set; }

        public float CurrentOutInRate { get; set; }

        public bool Active { get; set; }


        public List<WebmoneyProfieBid> GetBids()
        {
            var userId = (int)Membership.GetUser().ProviderUserKey;
            var bids = new List<WebmoneyProfieBid>();
            {
                using (var db = new UsersContext())
                {
                    bids = (from u in db.WebmoneyProfiesBids
                        where u.UserProfile.UserId == userId
                        select u).ToList();
                }
            }

            return bids;
        }

        public ResultAction Save()
        {
            ResultAction result = new ResultAction();
            var userId = (int)Membership.GetUser().ProviderUserKey;
            using (var db = new UsersContext())
            {
                var bid = new WebmoneyProfieBid();
                var userProfile = (from u in db.UserProfiles
                        where u.UserId == userId
                        select u).Single();
               
                var exchangerManager = new Webmoney.Exchanger.ExchangerManager();
                var queryDetails = exchangerManager.ListWmidBids(userProfile.Wmid, null, QueryId, userProfile.WebmoneyKey);
                if (queryDetails.Queries != null && queryDetails.Queries.Queries.Any())
                {
                    bid.QueryId = QueryId;
                    bid.Active = Active;
                    bid.MinOutInRate = MinOutInRate;
                    bid.CurrentOutInRate = queryDetails.Queries.Queries.Where(q => q.Id == Convert.ToUInt32(QueryId)).Select(q => q.OutInRate).First();
                    bid.UserProfile = userProfile;

                    var queryDetail = queryDetails.Queries.Queries.First();
                    var pair = ExchangeType.DirectionToCurrencyPair(queryDetail.Direction);
                    bid.InCurrency = pair.InCurrency;
                    bid.OutCurrency = pair.OutCurrency;
                    db.WebmoneyProfiesBids.Add(bid);
                    db.SaveChanges();
                    result.IsSuccess = true;
                }
                else
                {
                    result.ErrorMessage = queryDetails.ReturnDescription;
                }
            }

            return result;
        }

        public ResultAction Update()
        {
            ResultAction result = new ResultAction();
            var userId = (int)Membership.GetUser().ProviderUserKey;
            using (var db = new UsersContext())
            {
                var userProfile = db.UserProfiles.Where(u => u.UserId == userId).First();
                var bid = (from u in db.WebmoneyProfiesBids
                        where u.UserProfile.UserId == userId
                        select u).First();

                var exchangerManager = new Webmoney.Exchanger.ExchangerManager();
                var queryDetails = exchangerManager.ListWmidBids(userProfile.Wmid, null, QueryId, userProfile.WebmoneyKey);
                if (queryDetails.Queries != null && queryDetails.Queries.Queries.Any())
                {
                    bid.Active = Active;
                    bid.MinOutInRate = MinOutInRate;

                    var queryDetail = queryDetails.Queries.Queries.First();
                    var pair = ExchangeType.DirectionToCurrencyPair(queryDetail.Direction);
                    bid.InCurrency = pair.InCurrency;
                    bid.OutCurrency = pair.OutCurrency;
                    db.SaveChanges();
                    result.IsSuccess = true;
                }
                else
                {
                    result.ErrorMessage = queryDetails.ReturnDescription;
//                    result.ErrorMessage = "Is not possible update this bid. It can occure if selected QuerId in wm.echanger is not active or does not exist. Please check this QuerId on wm.echanger side";
                }
            }

            return result;
        }

        public void Delete()
        {
            var userId = (int)Membership.GetUser().ProviderUserKey;
            using (var db = new UsersContext())
            {
                var bid = db.WebmoneyProfiesBids.Where(b => b.UserProfile.UserId == userId && b.QueryId == QueryId).First();

                db.WebmoneyProfiesBids.Remove(bid);
                db.SaveChanges();
            }
        }
    }
}